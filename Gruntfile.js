module.exports = function(grunt) {
    var manifest = grunt.file.readJSON('manifest.json');

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        clean: {
            dest: 'dest'
        },

        copy: {
            default:{
                files: [{
                    expand: true,
                    src: ['manifest.json', 'css/*', 'js/*', 'images/*', '*.html'],
                    dest: 'dest/'
                }]
            }
        },

        cssmin: {
            default: {
                options: {
                    report: 'gzip'
                },
                files: [{
                    expand: true,
                    cwd: 'dest/css/',
                    src: ['*.css'],
                    dest: 'dest/css/',
                    ext: '.css'
                }]
            }
        },

        uglify: {
            default: {
                options: {
                    preserveComments: false,
                    report: 'gzip'
                },
                files: [{
                    expand: true,
                    cwd: 'dest/js/',
                    src: ['*.js'],
                    dest: 'dest/js/',
                    ext: '.js'
                }]
            }
        },

        compress: {
            default: {
                options: {
                    archive: function () {
                        return 'dest/' + manifest.name + '-' + manifest.version + '.zip';
                    }
                },
                expand: true,
                cwd: 'dest/',
                src: ['**/*'],
                dest: '/'
            }
        }
    });

    // load
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-compress');

    // task
    grunt.registerTask('default', ['clean', 'copy', 'cssmin', 'uglify', 'compress']); // default
};