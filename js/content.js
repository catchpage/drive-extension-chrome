(function(window) {
    var CP = {};
    window.CP = CP;

    // getDescription
    CP.getDescription = function() {
        var description = '';
        var metas = document.getElementsByTagName('meta');
        for (var i = 0; i < metas.length; i++) {
            if (metas[i].name.toLowerCase().indexOf('description') >= 0) {
                description = metas[i].content;
                break;
            }
        }
        return description;
    };
})(window);